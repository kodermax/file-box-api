cls
cd ..
FOR /F "tokens=*" %%A in ('aws iam get-role --role-name lambda-role-execution --query "Role.Arn" --output text') do SET lambda_role_arn=%%A

"C:\Program Files\7-Zip\7z" a -r -tzip deployment.zip * -xr!*.git* -xr!*.md -xr!aws -xr!test -xr!docs\postman
set function_name="smith-poc-file-box-api"
aws lambda create-function ^
    --function-name %function_name% ^
    --zip-file fileb://deployment.zip ^
    --role %lambda_role_arn% ^
    --handler index.handler ^
    --runtime nodejs6.10 ^
    --environment Variables="{config='{\"log\":{\"level\":2},\"filebox\":{\"provider\":\"mock\"},\"s3\":{\"bucket\":\"file-box-poc\"},\"routePrefix\":\"/smith-poc-file-box-api\",\"acl\":{\"authCHeader\":\"x-api-key\",\"apiKeys\":[{\"apiKey\":\"e6ffd1e1-f423-4c2d-b82b-2473f673c2ba\",\"forceMock\":true,\"roles\":[\"full-access\"]},{\"apiKey\":\"fca92103-e03e-4e87-b30b-999822783335\",\"roles\":[\"full-access\"]},{\"apiKey\":\"c75ca992-4cba-4167-9703-c09a12c6684c\",\"roles\":[\"test\"]}],\"roles\":[{\"role\":\"full-access\",\"paths\":[\".*\"]},{\"role\":\"test\",\"paths\":[\"^/test/.*\"]}]}}'}"
    
del deployment.zip
